import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ConfigEditComponent} from './pages/config/config-edit/config-edit.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {ConfigControlNodeComponent} from './components/config/config-control/config-control-node/config-control-node.component';
import {PureConfigControlComponent} from './components/config/config-control/controls/pure-config-control/pure-config-control.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatRippleModule} from '@angular/material/core';
import {ConfigTreeNavigationComponent} from './components/config/config-tree-navigation/config-tree-navigation.component';
import {ConfigEditorComponent} from './components/config/config-editor/config-editor.component';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {AngularSplitModule} from 'angular-split';
import {NgSelectModule} from '@ng-select/ng-select';
import {ConfigControlNodeChildrenComponent} from './components/config/config-control/config-control-node-children/config-control-node-children.component';
import {UnknownControlComponent} from './components/config/config-control/controls/unknown-control/unknown-control.component';
import {ConfigInputControlComponent} from './components/config/config-control/controls/config-input-control/config-input-control.component';
import {ConfigSelectControlComponent} from './components/config/config-control/controls/config-select-control/config-select-control.component';
import {ConfigTextareaControlComponent} from './components/config/config-control/controls/config-textarea-control/config-textarea-control.component';
import {ConfigCheckboxControlComponent} from './components/config/config-control/controls/config-checkbox-control/config-checkbox-control.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MonacoEditorModule} from 'ngx-monaco-editor';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ConfigExpansionPanelControlComponent} from './components/config/config-control/controls/config-expansion-panel-control/config-expansion-panel-control.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { ProfilerComponent } from './pages/profiler/profiler.component';
import {ChartsModule} from 'ng2-charts';
import { OverviewComponent } from './components/profiler/diagrams/overview/overview.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfigEditComponent,
    ConfigControlNodeComponent,
    PureConfigControlComponent,
    ConfigTreeNavigationComponent,
    ConfigEditorComponent,
    ConfigControlNodeChildrenComponent,
    UnknownControlComponent,
    ConfigInputControlComponent,
    ConfigSelectControlComponent,
    ConfigTextareaControlComponent,
    ConfigCheckboxControlComponent,
    ConfigExpansionPanelControlComponent,
    ProfilerComponent,
    OverviewComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        // External Components
        AngularSplitModule,
        NgSelectModule,
        MonacoEditorModule.forRoot(),
        // Angular Material Modules
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatSidenavModule,
        MatListModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatTreeModule,
        MatRippleModule,
        MatTableModule,
        MatTabsModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatExpansionModule,
        ChartsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
