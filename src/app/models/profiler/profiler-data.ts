import {LazyGetter} from 'lazy-get-decorator';

export abstract class ProfilerNode {

  abstract get recursiveGetDataEntries(): ProfilerNodeDataEntry[];
}

export class ProfilerGroupNode implements ProfilerNode {
  public children: { [name: string]: ProfilerNode } = {};

  constructor(readonly name: string) {
  }

  @LazyGetter()
  get recursiveGetDataEntries() {
    return [...Object.values(this.children).map(child => child.recursiveGetDataEntries)
      .reduce((sum, next) => {
        next.forEach(child => sum.add(child));
        return sum;
      }, new Set<ProfilerNodeDataEntry>())];
  }
}

export class ProfilerLeafNode implements ProfilerNode {
  public data: ProfilerNodeDataEntry[] = [];

  constructor() {
  }

  get recursiveGetDataEntries() {
    return this.data;
  }
}

export interface ProfilerNodeDataEntry {
  time: number;
  tick: number;
}


export class ProfilerData {
  root: ProfilerGroupNode = new ProfilerGroupNode('root');

  @LazyGetter() get minTick() {
    return Math.min(...this.root.recursiveGetDataEntries.map(it => it.tick));
  }

  @LazyGetter() get maxTick() {
    return Math.max(...this.root.recursiveGetDataEntries.map(it => it.tick));
  }

  @LazyGetter() get timeline() {
    return [...Array(this.maxTick - this.minTick + 1).keys()].map(it => it + this.minTick);
  }

  @LazyGetter()
  get overviewData() {
    return Object.entries(this.root.children).map(([key, groupNode]) => ({
      label: key,
      data: this.timeline
        .map(tick => groupNode.recursiveGetDataEntries.filter(data => data.tick === tick)
          .reduce((sum, data) => sum + data.time, 0) / 1000000),
      categoryPercentage: 1.0,
      barPercentage: 1.0
    }));
  }
}
