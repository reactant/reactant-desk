import {ConfigClientControl} from './config-client-control';

export interface ConfigNodeSnapshot {
  identifier: string;
  name: string;
  type: ConfigNodeType;
  children?: { [identifier: string]: ConfigNodeSnapshot };
}


export interface ConfigGroupNodeSnapshot extends ConfigNodeSnapshot {
  identifier: string;
  name: string;
}

export interface ConfigSingleNodeSnapshot extends ConfigNodeSnapshot {
  identifier: string;
  name: string;
}

export interface ConfigMultiNodeSnapshot extends ConfigNodeSnapshot {
  identifier: string;
  name: string;
  objects: { [identifier: string]: string };
}

export type ConfigNodeType = 'group' | 'single' | 'multi';
