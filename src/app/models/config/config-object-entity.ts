import {ConfigClientControl} from './config-client-control';

export interface ConfigObjectEntity {
  config: any;
  controlRoot: ConfigClientControl;
}
