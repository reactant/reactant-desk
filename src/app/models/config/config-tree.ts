import {ConfigMultiNodeSnapshot, ConfigNodeSnapshot} from './config-node-snapshot';

export class ConfigTree {
  readonly root: ConfigTreeNode;

  constructor(root: ConfigNodeSnapshot) {
    this.root = new ComplexConfigTreeNode(root);
  }
}

export interface ConfigTreeNode {
  identifier: string;
  name: string;
  nestedLevel: number;
  isConfig: boolean;
  isFolder: boolean;
  children: ConfigTreeNode[];
  fullIdentifier: string;
}

export class ComplexConfigTreeNode implements ConfigTreeNode {
  isConfig: boolean;
  isFolder: boolean;

  children: ConfigTreeNode[];
  nestedLevel: number;
  identifier: string;
  name: string;
  fullIdentifier: string;

  constructor(source: ConfigNodeSnapshot, readonly parent?: ConfigTreeNode) {
    this.nestedLevel = (parent?.nestedLevel ?? -1) + 1;
    this.identifier = source.identifier;
    this.isConfig = source.type === 'single';
    this.isFolder = source.type === 'multi' || source.type === 'group';
    this.name = source.name;
    this.fullIdentifier = this.nestedLevel === 0 ? '' : (this.parent?.fullIdentifier ?? '') + '/' + this.identifier;

    if (source.type === 'multi') {
      this.children = Object.entries((source as ConfigMultiNodeSnapshot).objects).map(entry => {
        console.log(this.fullIdentifier + '/' + entry[0]);
        return {
          identifier: entry[0],
          fullIdentifier: this.fullIdentifier + '/' + entry[0],
          name: entry[1],
          nestedLevel: this.nestedLevel + 1,
          isConfig: true, isFolder: false, children: []
        } as ConfigTreeNode;
      }).sort((a, b) => a.name.localeCompare(b.name));
    } else if (source.type === 'group') {
      this.children = (Object.values(source.children ?? {})).map(it => new ComplexConfigTreeNode(it, this));
    }
  }

}
