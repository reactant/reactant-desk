export interface ConfigClientControl {
  controlType: string;
  attributes: {
    'displayName': string,
    'description': string,
    'clientSideValidationCode': string,
    'propertyKey': string,
    'nullable': boolean,
    [key: string]: any
  };
  children: ConfigClientControl[];
}

export type ControlType = 'pure' | 'list' | 'expansion-panel' | 'textarea' | 'select' | 'input' | 'checkbox';
