import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfilerDataService} from '../../services/api/profiler-data.service';
import {ProfilerData} from '../../models/profiler/profiler-data';
import {filter, flatMap, map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-profiler',
  templateUrl: './profiler.component.html',
  styleUrls: ['./profiler.component.scss']
})
export class ProfilerComponent implements OnInit {
  profilerData: ProfilerData = null;
  loading = true;

  constructor(private route: ActivatedRoute, private profilerDataService: ProfilerDataService) {
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(
      map(queryParams => queryParams.data),
      filter(sourceUrl => sourceUrl !== undefined),
      tap(() => this.loading = true),
      flatMap(sourceUrl => this.profilerDataService.getProfilerData(sourceUrl))
    ).subscribe(profilerData => {
      this.profilerData = profilerData;
      this.loading = false;
      console.log(this.profilerData);
    });
  }

}
