import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfigApiService} from '../../../services/api/config-api.service';
import {ConfigObjectEntity} from '../../../models/config/config-object-entity';
import {ConfigTree} from '../../../models/config/config-tree';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-config-edit',
  templateUrl: './config-edit.component.html',
  styleUrls: ['./config-edit.component.scss']
})
export class ConfigEditComponent implements OnInit {

  constructor(private route: ActivatedRoute, private configApiService: ConfigApiService, private router: Router) {
  }

  configTree?: BehaviorSubject<ConfigTree> = new BehaviorSubject(null);

  openedConfigTabs: { configFullIdentifier: string, config: ConfigObjectEntity }[] = [];
  focusingTabIndex = 0;

  ngOnInit(): void {
    this.loadConfigTree();
  }

  loadConfigTree() {
    this.configApiService.getConfigTree().subscribe(tree => this.configTree.next(tree));
  }

  getOpenedConfigTabIndex(configFullIdentifier: string): number | undefined {
    return this.openedConfigTabs.map((it, i) => ({index: i, tab: it}))
      .find(it => it.tab.configFullIdentifier === configFullIdentifier)?.index;
  }

  openConfig(configFullIdentifier: string) {
    const openedIndex = this.getOpenedConfigTabIndex(configFullIdentifier);
    if (openedIndex != null) {
      this.focusingTabIndex = openedIndex;
    } else {
      this.configApiService.getConfig(configFullIdentifier)
        .subscribe(config => {
          this.openedConfigTabs.push({configFullIdentifier, config});
          this.focusingTabIndex = this.openedConfigTabs.length - 1;
        });
    }
  }

}
