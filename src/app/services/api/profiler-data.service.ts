import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProfilerData, ProfilerGroupNode, ProfilerLeafNode, ProfilerNodeDataEntry} from '../../models/profiler/profiler-data';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfilerDataService {
  constructor(private http: HttpClient) {

  }

  getProfilerData(url: string): Observable<ProfilerData> {
    return this.http.get<any>(url).pipe(map(data => {

      const result: ProfilerData = new ProfilerData();

      Object.entries(data).forEach(([key, value]) => {
        const dataProviderGroupNode = new ProfilerGroupNode(key);
        result.root.children[key] = dataProviderGroupNode;


        Object.entries(value).forEach(([groupsArrKey, groupedValue]) => {
          const query = groupsArrKey.slice(1, -1).split(',');
          const targetGroup = this.getOrCreate(dataProviderGroupNode, query);

          Object.entries(groupedValue as { [timeUser: string]: ProfilerNodeDataEntry[] })
            .forEach(([timeUser, dataEntries]) => {
              const leaf = new ProfilerLeafNode();
              targetGroup.children[timeUser] = leaf;
              leaf.data = dataEntries;
            });

        });
      });

      return result;

    }));
  }

  private getOrCreate(group: ProfilerGroupNode, query: Array<string>): ProfilerGroupNode {
    if (query.length === 0) {
      return group;
    }
    const nextQuery = [...query];
    const next = nextQuery.splice(0, 1)[0];
    if (group.children[next] === undefined) {
      group.children[next] = new ProfilerGroupNode(next);
    }
    return this.getOrCreate(group.children[next] as ProfilerGroupNode, nextQuery);
  }

}
