import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeskConfigService} from '../desk-config.service';
import {ConfigNodeSnapshot} from '../../models/config/config-node-snapshot';
import {ConfigObjectEntity} from '../../models/config/config-object-entity';
import {ConfigTree} from '../../models/config/config-tree';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigApiService {

  constructor(private deskConfigService: DeskConfigService, private httpClient: HttpClient) {
  }

  getConfigTree(): Observable<ConfigTree> {
    return this.httpClient.get<ConfigNodeSnapshot>(`${this.deskConfigService.apiUrl}/config`)
      .pipe(map(it => new ConfigTree(it)));
  }

  getConfig(path: string): Observable<ConfigObjectEntity> {
    return this.httpClient.get<ConfigObjectEntity>(`${this.deskConfigService.apiUrl}/config/${path}`).pipe(
      tap(it => it.controlRoot.attributes.propertyKey = 'config')
    );
  }

}

