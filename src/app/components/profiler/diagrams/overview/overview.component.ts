import {Component, Input, OnInit} from '@angular/core';
import {ProfilerData} from '../../../../models/profiler/profiler-data';
import {ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  @Input()
  profilerData: ProfilerData;

  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      align: 'start'
    },
    scales: {
      xAxes: [{}], yAxes: [{}]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  private barChartLabelsCache: Label[];

  get barChartLabels(): Label[] {
    return this.barChartLabelsCache || (this.barChartLabelsCache = this.profilerData.timeline.map(it => `${it}`));
  }

  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  private barChartDataCache;

  get barChartData() {
    return this.profilerData.overviewData;
  }

  width = 100;

  scroll(event: any) {
    this.width += (event.deltaY / 10.0);
    this.width = Math.max(this.width, 100);
    console.log(this.width);
  }


  constructor() {
  }

  ngOnInit(): void {
  }

}
