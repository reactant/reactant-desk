import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {ConfigTree, ConfigTreeNode} from '../../../models/config/config-tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {Observable} from 'rxjs';

interface ConfigFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-config-tree-navigation',
  templateUrl: './config-tree-navigation.component.html',
  styleUrls: ['./config-tree-navigation.component.scss']
})
export class ConfigTreeNavigationComponent implements OnInit {

  @Input()
  tree: Observable<ConfigTree>;

  @Output()
  configOpen = new EventEmitter<string>();

  treeControl = new FlatTreeControl<ConfigFlatNode>(node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    (node: ConfigTreeNode, level: number) => ({
      expandable: !!node.children && node.children.length > 0, name: node.name, level, treeNode: node
    }), node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
  }

  ngOnInit(): void {
    this.tree.subscribe(tree => {
      console.log(tree);
      return this.dataSource.data = tree == null ? [] : tree.root.children;
    });
  }

  hasChild = (_: number, node: ConfigFlatNode) => node.expandable;
}
