import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ConfigObjectEntity} from '../../../models/config/config-object-entity';

@Component({
  selector: 'app-config-editor',
  templateUrl: './config-editor.component.html',
  styleUrls: ['./config-editor.component.scss']
})
export class ConfigEditorComponent implements OnInit {

  @Input()
  config?: ConfigObjectEntity;

  showSource = false;
  sourceCode = '';

  editorOptions = {theme: 'vs-dark', language: 'json', automaticLayout: true};

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
  }

  toggleSource($event) {
    this.showSource = $event.checked;
    if (this.showSource) {
      this.sourceCode = JSON.stringify(this.config, null, 2);
    } else {
      this.config = JSON.parse(this.sourceCode);
    }
    this.changeDetectorRef.detectChanges();
  }
}
