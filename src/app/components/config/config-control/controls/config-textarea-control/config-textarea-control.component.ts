import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-textarea-control',
  templateUrl: './config-textarea-control.component.html',
  styleUrls: ['./config-textarea-control.component.scss']
})
export class ConfigTextareaControlComponent implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() {
  }

  ngOnInit(): void {
  }

}
