import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-unknown-control',
  templateUrl: './unknown-control.component.html',
  styleUrls: ['./unknown-control.component.scss']
})
export class UnknownControlComponent implements OnInit, ConfigControlComponent {

  constructor() {
  }

  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  ngOnInit(): void {
  }

}
