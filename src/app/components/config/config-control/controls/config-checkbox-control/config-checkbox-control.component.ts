import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-checkbox-control',
  templateUrl: './config-checkbox-control.component.html',
  styleUrls: ['./config-checkbox-control.component.scss']
})
export class ConfigCheckboxControlComponent implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() {
  }

  ngOnInit(): void {
  }

}
