import {ConfigClientControl} from '../../../../models/config/config-client-control';
export {ConfigClientControl} from '../../../../models/config/config-client-control';
import {PureConfigControlComponent} from './pure-config-control/pure-config-control.component';
export {PureConfigControlComponent} from './pure-config-control/pure-config-control.component';
import {ConfigCheckboxControlComponent} from './config-checkbox-control/config-checkbox-control.component';
export {ConfigCheckboxControlComponent} from './config-checkbox-control/config-checkbox-control.component';
import {ConfigInputControlComponent} from './config-input-control/config-input-control.component';
export {ConfigInputControlComponent} from './config-input-control/config-input-control.component';
import {ConfigSelectControlComponent} from './config-select-control/config-select-control.component';
export {ConfigSelectControlComponent} from './config-select-control/config-select-control.component';
import {ConfigTextareaControlComponent} from './config-textarea-control/config-textarea-control.component';
export {ConfigTextareaControlComponent} from './config-textarea-control/config-textarea-control.component';
import {ConfigExpansionPanelControlComponent} from './config-expansion-panel-control/config-expansion-panel-control.component';
export {ConfigExpansionPanelControlComponent} from './config-expansion-panel-control/config-expansion-panel-control.component';


export interface ConfigControlComponent {
  controlNode: ConfigClientControl;
  data: any;
}

export const CONTROL_TYPE_MAP = {
  pure: PureConfigControlComponent,
  checkbox: ConfigCheckboxControlComponent,
  input: ConfigInputControlComponent,
  select: ConfigSelectControlComponent,
  textarea: ConfigTextareaControlComponent,
  'expansion-panel': ConfigExpansionPanelControlComponent
};
