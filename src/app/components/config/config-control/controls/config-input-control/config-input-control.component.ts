import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-input-control',
  templateUrl: './config-input-control.component.html',
  styleUrls: ['./config-input-control.component.scss']
})
export class ConfigInputControlComponent implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() { }

  ngOnInit(): void {
  }

}
