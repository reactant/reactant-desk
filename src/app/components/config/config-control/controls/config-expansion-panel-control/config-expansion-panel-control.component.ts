import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-expansion-panel-control',
  templateUrl: './config-expansion-panel-control.component.html',
  styleUrls: ['./config-expansion-panel-control.component.scss']
})
export class ConfigExpansionPanelControlComponent implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() {
  }

  ngOnInit(): void {
  }

}
