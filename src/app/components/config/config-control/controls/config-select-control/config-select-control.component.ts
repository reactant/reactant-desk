import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-select-control',
  templateUrl: './config-select-control.component.html',
  styleUrls: ['./config-select-control.component.scss']
})
export class ConfigSelectControlComponent  implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() { }

  ngOnInit(): void {
  }

}
