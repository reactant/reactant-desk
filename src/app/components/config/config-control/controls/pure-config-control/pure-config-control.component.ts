import {Component, Input, OnInit} from '@angular/core';
import {ConfigControlComponent, CONTROL_TYPE_MAP} from '../config-control.component';
import {ConfigClientControl} from '../../../../../models/config/config-client-control';

@Component({
  selector: 'app-pure-config-control',
  templateUrl: './pure-config-control.component.html',
  styleUrls: ['./pure-config-control.component.scss']
})
export class PureConfigControlComponent implements OnInit, ConfigControlComponent {
  @Input()
  controlNode: ConfigClientControl;
  @Input()
  data: any;

  constructor() {
  }

  ngOnInit(): void {
  }


}
