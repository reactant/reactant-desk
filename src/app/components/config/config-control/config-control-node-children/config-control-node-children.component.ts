import {Component, Input, OnInit} from '@angular/core';
import {ConfigClientControl} from '../../../../models/config/config-client-control';

@Component({
  selector: 'app-config-control-node-children',
  templateUrl: './config-control-node-children.component.html',
  styleUrls: ['./config-control-node-children.component.scss']
})
export class ConfigControlNodeChildrenComponent implements OnInit {
  @Input()
  controlNode: ConfigClientControl;

  @Input()
  data: any;

  constructor() {
  }

  ngOnInit(): void {
  }

}
