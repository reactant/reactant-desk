import {Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ConfigClientControl} from '../../../../models/config/config-client-control';
import {ConfigControlComponent, CONTROL_TYPE_MAP} from '../controls/config-control.component';
import {UnknownControlComponent} from '../controls/unknown-control/unknown-control.component';

@Component({
  selector: 'app-config-control-node',
  templateUrl: './config-control-node.component.html',
  styleUrls: ['./config-control-node.component.scss']
})
export class ConfigControlNodeComponent implements OnInit {
  @Input()
  controlNode: ConfigClientControl;

  @Input()
  data: any;

  @ViewChild('controlHost', {static: true, read: ViewContainerRef})
  controlView: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    const controlComponent = CONTROL_TYPE_MAP[this.controlNode.controlType] ?? UnknownControlComponent;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(controlComponent);
    const component = this.controlView.createComponent(componentFactory) as ComponentRef<ConfigControlComponent>;
    component.instance.controlNode = this.controlNode;
    component.instance.data = this.data;
  }

}
