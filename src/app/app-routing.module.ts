import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfigEditComponent} from './pages/config/config-edit/config-edit.component';
import {ProfilerComponent} from './pages/profiler/profiler.component';


const routes: Routes = [
  {path: 'config', component: ConfigEditComponent},
  {path: 'profiler', component: ProfilerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
